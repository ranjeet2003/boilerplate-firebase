const { initializeApp } = require("firebase-admin/app");

initializeApp({
  credential: applicationDefault(),
  databaseURL: "https://boilerplate-642b3-default-rtdb.firebaseio.com",
});
